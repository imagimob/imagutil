import pandas as pd
import numpy as np
import argparse
import os

DESCRIPTION = 'Convert one label per row to imagimob label format.'

TIME_HEADER = 'Time(Seconds)'
LABEL_LENGTH_HEADER = 'Length(Seconds)'
LABEL_LABEL_HEADER = 'Label(string)'
LABEL_CONFIDENCE_HEADER = 'Confidence(double)'
LABEL_COMMENT_HEADER = 'Comment(string)'

DATA_FILE_NAME = 'data_updated.csv'
LABEL_FILE_NAME = 'label.csv'


def convert_row_labels_to_imagimob_labels(data_frame, label_column_name, time_column_name):
    """
    Convert one label per row to imagimob label format.
    """
    time_np = data_frame[time_column_name].to_numpy()
    label_np = data_frame[[time_column_name, label_column_name]]

    new_time = []
    new_length = []
    new_label = []
    new_confidence = []
    new_comment = []

    new_time.append(time_np[0])
    new_label.append(label_np[label_column_name][0])
    new_confidence.append(0)
    new_comment.append('')

    start_time = new_time[-1]
    prev_label = new_label[-1]
    
    for i in range(1, time_np.shape[0]):
        label = label_np[label_column_name][i]
        if label != prev_label:

            if pd.isna(prev_label) is False:
                new_length.append(time_np[i-1] - start_time)

            if pd.isna(label):
                start_time = time_np[i]
                prev_label = label
                continue

            new_time.append(time_np[i])
            new_label.append(label)
            new_confidence.append(0)
            new_comment.append('')

            start_time = new_time[-1]
            prev_label = new_label[-1]


    new_length.append(time_np[i-1] - new_time[-1])

    new_columns = [TIME_HEADER, LABEL_LENGTH_HEADER, LABEL_LABEL_HEADER, LABEL_CONFIDENCE_HEADER, LABEL_COMMENT_HEADER]
    labels_df = pd.DataFrame(list(zip(new_time, new_length, new_label, new_confidence, new_comment)), columns=new_columns)

    return labels_df

def save_files(data_df, labels_df, output_path):
    data_output_file = os.path.join(output_path, DATA_FILE_NAME)
    label_output_file = os.path.join(output_path, LABEL_FILE_NAME)
    data_output_file = data_output_file.replace('\\', '/')
    label_output_file = label_output_file.replace('\\', '/')

    data_df.to_csv(data_output_file, index=None)
    labels_df.to_csv(label_output_file, index=None)

# Remove file name from path and return that path
def getOutputPathFromFilePath(file_path):
    path = file_path.replace('\\', '/').split('/')
    file_name_length = len(path[-1])
    return file_path[:-file_name_length]

def main():
    args = parse_args()
    file_path = args.input_file
    output_path = args.output_path
    label_column_index = args.label_column_index
    label_column_name = args.label_column_name
    time_column_name = args.time_column_name

    df = pd.read_csv(file_path)

    # Get label column name from index
    if label_column_name is None:
        label_column_name = df.columns[label_column_index]
    # If output path is none then save it in same directory as the input data
    if output_path is None:
        output_path = getOutputPathFromFilePath(file_path)

    labels_df = convert_row_labels_to_imagimob_labels(df, label_column_name, time_column_name)
    data_df = df.drop(label_column_name, axis=1)

    save_files(data_df, labels_df, output_path)

    return 0

def parse_args():
    parser = argparse.ArgumentParser(DESCRIPTION)
    parser.add_argument(
        '--input-file',
        required=True,
        type=str,
        help='Path to file to be converted.')
    parser.add_argument(
        '--output-path',
        required=False,
        type=str,
        help='Path to directory to save updated data and labels.')
    parser.add_argument(
        '--label-column-index',
        required=False,
        default=-1,
        type=int,
        help='Index of the label column.')
    parser.add_argument(
        '--label-column-name',
        required=False,
        type=str,
        help='Name of the label column, will overwrite label-column-index.')
    parser.add_argument(
        '--time-column-name',
        required=False,
        default='Time [s]',
        type=str,
        help='Name of the time column.')
    return parser.parse_args()

if __name__ == '__main__':
    main()