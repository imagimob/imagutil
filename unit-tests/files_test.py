from imagutil.files import get_data_frame_list, get_data_file_paths, getFolderPathsFromFilePaths, get_output_paths_from_input_paths, get_output_paths_from_input_and_output_path
import unittest
import os

TEST_DIR = os.path.join(os.path.dirname(__file__))


class TestDataConverter(unittest.TestCase):

    def test_get_file_paths_in_one_folder(self):
        root_path = os.path.join(TEST_DIR, 'data{}path1{}'.format(os.sep, os.sep))
        file_paths = get_data_file_paths(root_path)
        expected = [os.path.join(root_path, 'data1.csv'), os.path.join(root_path, 'data2.csv')]
        self.assertEqual(file_paths, expected)
        pass

    def test_get_file_paths_in_sub_folders(self):
        root_path = os.path.join(TEST_DIR, 'data{}path2{}'.format(os.sep, os.sep))
        file_paths = get_data_file_paths(root_path)
        expected = [os.path.join(root_path, 'rec1', 'data.csv'), os.path.join(root_path, 'rec2', 'data.csv')]
        self.assertEqual(file_paths, expected)
        pass

    def test_get_folder_path_from_file_paths(self):
        file_path = [os.path.join(TEST_DIR, 'data{}path2{}rec1{}data.csv'.format(os.sep, os.sep, os.sep))]
        output = getFolderPathsFromFilePaths(file_path)
        expected = [os.path.join(TEST_DIR, 'data{}path2{}rec1'.format(os.sep, os.sep))]
        self.assertEqual(output, expected)
        pass

    def test_get_folder_path_from_file_paths_in_subfolders(self):
        file_paths = [os.path.join(TEST_DIR, 'data{}path2{}rec1{}data.csv'.format(os.sep, os.sep, os.sep)),
                      os.path.join(TEST_DIR, 'data{}path2{}rec2{}data.csv'.format(os.sep, os.sep, os.sep))]
        output = getFolderPathsFromFilePaths(file_paths)
        expected = [os.path.join(TEST_DIR, 'data{}path2{}rec1'.format(os.sep, os.sep)),
                    os.path.join(TEST_DIR, 'data{}path2{}rec2'.format(os.sep, os.sep))]
        self.assertEqual(output, expected)
        pass

    def test_get_output_paths_from_input_paths(self):
        file_paths = [os.path.join(TEST_DIR, 'data{}path1{}data1.csv'.format(os.sep, os.sep, os.sep)),
                      os.path.join(TEST_DIR, 'data{}path1{}data2.csv'.format(os.sep, os.sep, os.sep))]
        output = get_output_paths_from_input_paths(file_paths)

        expected = [os.path.join(TEST_DIR, 'data{}path1{}data1'.format(os.sep, os.sep)),
                    os.path.join(TEST_DIR, 'data{}path1{}data2'.format(os.sep, os.sep))]
        self.assertEqual(output, expected)
        pass

    def test_get_output_paths_from_input_paths_in_subfolders(self):
        file_paths = [os.path.join(TEST_DIR, 'data{}path2{}rec1{}data.csv'.format(os.sep, os.sep, os.sep)),
                      os.path.join(TEST_DIR, 'data{}path2{}rec2{}data.csv'.format(os.sep, os.sep, os.sep))]
        output = get_output_paths_from_input_paths(file_paths)

        expected = [os.path.join(TEST_DIR, 'data{}path2{}rec1'.format(os.sep, os.sep)),
                    os.path.join(TEST_DIR, 'data{}path2{}rec2'.format(os.sep, os.sep))]
        self.assertEqual(output, expected)
        pass

    def test_get_output_paths_from_output_paths_and_input_paths(self):
        file_paths = [os.path.join(TEST_DIR, 'data{}path1{}data1.csv'.format(os.sep, os.sep)),
                      os.path.join(TEST_DIR, 'data{}path1{}data2.csv'.format(os.sep, os.sep))]
        output_root_path = os.path.join(TEST_DIR, 'data{}output_path{}'.format(os.sep, os.sep))
        output = get_output_paths_from_input_and_output_path(file_paths, output_root_path)

        expected = [os.path.join(TEST_DIR, 'data{}output_path{}data1'.format(os.sep, os.sep)),
                    os.path.join(TEST_DIR, 'data{}output_path{}data2'.format(os.sep, os.sep))]

        self.assertEqual(output, expected)
        pass

    def test_get_output_paths_from_output_paths_and_input_paths_in_subfolders(self):
        # TODO: Continue writing tests
        pass

if __name__ == '__main__':
    unittest.main()