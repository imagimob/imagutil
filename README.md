# README #

This repository contains the Imagutil python library as well as common tools and scripts that do not have any other place to live.

## How do I install Imagutil in my python environment? ##

There are to main ways to install the library, one for users that just want to import the library and one for developers that want to develop in this repo.

### User installation ###
The Imagutil library is most easily installed via pip. The below command will install the Imagutil library from the current master:
```
pip install git+https://git@bitbucket.org/imagimob/imagutil.git
```

### Developer installation ###
If you want to develop scripts/code in this repo it is possible and recommended to install the Imagutil library in edit mode. This means that if you change the code in this repo, it will take effect in the "installed" library in your python environment. This can be done with the below command:

```bash
pip install -e .
```

## How do I run the Imagutil scripts? ##

When you have installed imagutil the scripts will be available in your environment and can be called as the below example:
  
```bash
python -m imagutil.scripts.split_recordings --input-dir example/input/dir --output-dir example/output/dir --length 120
```

## How do I use the Imagutil library in my code? ##

When you have installed imagutil you should be able to import it in your code like any other python library. E.g.:

```python
from imagutil.recording_split import split_recordings_with_mirrored_structure

input_dir = 'example/input/dir'
output_dir = 'example/output/dir'
recording_length = 120

split_recordings_with_mirrored_structure(input_dir, output_dir, recording_length)
```

## What folder structures are supported for imagimob_data_converter.py script? ##
The following folder structures are supported however they are mutual exclusive with each other.

/data-folder/
    /data1.csv
    /data2.csv
    /data3.csv

/data-folder/
    /data1-folder/data.csv
    /data2-folder/data.csv
    /data3-folder/data.csv

## How do I contribute to the Imagutil repo? ##

The following principles should be applied when developing to the Imagutil repo:

### Git workflow ###
You should not commit and push your work directly on the master branch. Instead you should push it to feature branches. In bash terms, the git flow should look something like this:
  
```bash
git checkout -b my-feature
git add my_script.py
git commit -m "Added new amazing script"
git push my-feature
```
  
When you have pushed your work and are satisfied with how it looks, you should create a pull request in Bitbucket and ask a colleague to review it!

### Coding guidelines ###
To have a coherent looking and easy to understand repo we should write our code according to the same principles.

*TODO:* The coding principles need to be agreed upon

### Repo structure ###
The repo structure follows the recommended python standard for installable libraries:

```bash
- imagutil            # repo root directory
-- imagutil           # contains the imagutil python library
-- imagutil/scripts   # contains python command line scripts
-- test               # contains tests for the imagutil library
```
