import setuptools

def parse_requirements(filename):
    """ Load requirements from a pip requirements file """
    lineiter = (line.strip() for line in open(filename))
    return [line for line in lineiter if line and not line.startswith("#")]

# Parse README to add as help text
with open('README.md', encoding='utf-8') as fh:
    long_description = fh.read()

# Parse requirements.txt
install_reqs = parse_requirements('requirements.txt')

setuptools.setup(
    name="imagutil",
    version="1.0.6",
    description="Imagimob utility library",
    long_description=long_description,
    long_description_content_type='text/markdown',
    install_requires=install_reqs,
    packages=setuptools.find_packages(where='.'),
    python_requires='>=3.6',
)
