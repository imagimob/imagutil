import os
from pathlib import Path
import pandas as pd

from .headers import TIME_HEADER, LABEL_LENGTH_HEADER, LABEL_LABEL_HEADER
from .headers import LABEL_CONFIDENCE_HEADER, LABEL_COMMENT_HEADER



def search_for_recording_directories(search_dir: Path):
    """
    Searches for recording directories by searching recursively for .imsession files
    and returning the parent path.
    """
    data_files = list(search_dir.rglob('*' + ".data"))
    recording_dirs = [p.parents[0] for p in data_files]
    return recording_dirs

def create_label_dataframe():
    """
    Create a pandas.DataFrame object with the default Imagimob Studio labels format
    """
    columns = [TIME_HEADER, LABEL_LENGTH_HEADER, LABEL_LABEL_HEADER, LABEL_CONFIDENCE_HEADER, LABEL_COMMENT_HEADER]
    df = pd.DataFrame(columns=columns)
    df.set_index(TIME_HEADER, inplace=True)
    return df

def add_empty_label_track(root_input_dir: Path,track_name: str):
    
    data_files_dirs = search_for_recording_directories(root_input_dir)
    if len(data_files_dirs) == 0:
        print(f"Could not find any data files with given path: {root_input_dir}")
        return
    print("\n")
    print(f"Generating empty label tracks in {len(data_files_dirs)} directory")

    for data_file_dir in data_files_dirs:
        label_track_df = create_label_dataframe()
        label_file_path = data_file_dir / (track_name)
        label_track_df.to_csv(label_file_path)

        