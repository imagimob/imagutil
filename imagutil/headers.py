from .files import DATA_EXT, LABEL_EXT
import pandas as pd
import glob
import os

"""
Contains commonly used header names and functions
"""

TIME_HEADER = 'Time(Seconds)'

LABEL_LENGTH_HEADER = 'Length(Seconds)'
LABEL_LABEL_HEADER = 'Label(string)'
LABEL_CONFIDENCE_HEADER = 'Confidence(double)'
LABEL_COMMENT_HEADER = 'Comment(string)'

def addHeadersToData(df, timestamp_column_index):
    columns = df.shape[1]
    headers =  ['f{}'.format(i) for i in range(columns)]
    if timestamp_column_index is not None:
        headers[timestamp_column_index] = TIME_HEADER
    df.columns = headers
    return df

def addHeadersToDataFrames(dfs, timestamp_column_index):
    df_updated = []
    for df in dfs:
        df = addHeadersToData(df, timestamp_column_index)
        df_updated.append(df)
    return df_updated

def createOutputPath(input_path, root_output_path):
    input_path = input_path.replace('\\', '/')
    input_path = input_path.split('/')
    file_folder = input_path[-2]
    file_name = input_path[-1]
    output_path = os.path.join(root_output_path, file_folder)
    return output_path, file_name

def saveData(df, output_path, file_name):
    if not os.path.exists(output_path):
        os.makedirs(output_path)
    df.to_csv(os.path.join(output_path, file_name), index=False)
    return

def addHeadersToFiles(root_intput_path, root_output_path, timestamp_column_index, file_name, data_extension=DATA_EXT):
    input_paths = glob.glob(root_intput_path + '/*/*' + data_extension)
    for path in input_paths:
        df = pd.read_csv(path, header=None)
        df = addHeadersToData(df, timestamp_column_index)
        output_path, new_file_name = createOutputPath(path, root_output_path)
        if file_name is None:
            file_name = new_file_name
        saveData(df, output_path, file_name)
    return