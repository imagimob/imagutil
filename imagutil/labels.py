import numpy as np
import pandas as pd
from .headers import TIME_HEADER, LABEL_LENGTH_HEADER, LABEL_LABEL_HEADER, LABEL_CONFIDENCE_HEADER, LABEL_COMMENT_HEADER

def convert_labels(data_frame, labels_column_name, labels_map):
    """
    Convert each label in the 'labels_column_name' column into a new coresponding label in labels_map.
    """
    data_frame[labels_column_name] = data_frame[labels_column_name].apply(lambda x, l : l[x], args=((labels_map,)))
    return data_frame

def convert_row_labels_to_imagimob_labels(data_frame):
    """
    Convert one label per row to imagimob label format.
    """
    columns = data_frame.columns

    time_np = data_frame[TIME_HEADER].to_numpy()
    label_np = data_frame.drop(TIME_HEADER, axis=1).to_numpy()

    new_time = []
    new_length = []
    new_label = []
    new_confidence = []
    new_comment = []

    new_time.append(time_np[0])
    new_label.append(label_np[0][0])
    new_confidence.append(0)
    new_comment.append('')
    
    for i in range(1, time_np.shape[0]):
        if label_np[i] != new_label[-1]:
            new_length.append(time_np[i-1] - new_time[-1])

            new_time.append(time_np[i])
            new_label.append(label_np[i][0])
            new_confidence.append(0)
            new_comment.append('')
    new_length.append(time_np[i-1] - new_time[-1])

    new_columns = [TIME_HEADER, LABEL_LENGTH_HEADER, LABEL_LABEL_HEADER, LABEL_CONFIDENCE_HEADER, LABEL_COMMENT_HEADER]
    new_df = pd.DataFrame(list(zip(new_time, new_length, new_label, new_confidence, new_comment)), columns=new_columns)

    return new_df

def convert_row_based_labels_to_imagimob_labels(data_frame, label_column_name, time_column_name, default_label=None):
    """
    Convert one label per row to imagimob label format.
    """
    time_np = data_frame[time_column_name].to_numpy()
    label_np = data_frame[[time_column_name, label_column_name]]

    start_index = 0

    new_time = []
    new_length = []
    new_label = []
    new_confidence = []
    new_comment = []

    if default_label is not None:
        for i in range(1, time_np.shape[0]):
            if str(label_np[label_column_name][start_index]) != str(default_label):
                new_time.append(time_np[start_index])
                new_label.append(label_np[label_column_name][start_index])
                new_confidence.append(0)
                new_comment.append('')
                break
            start_index = start_index + 1
        start_index = start_index + 1
    else:
        start_index = 0
        new_time.append(time_np[start_index])
        new_label.append(label_np[label_column_name][start_index])
        new_confidence.append(0)
        new_comment.append('')
    

    if len(new_time) > 0:
        start_time = new_time[-1]
        prev_label = new_label[-1]
        
        for i in range(start_index, time_np.shape[0]):
            label = label_np[label_column_name][i]
            if label != prev_label:

                if pd.isna(prev_label) is False and default_label is None:
                    new_length.append(time_np[i-1] - start_time)
                elif pd.isna(prev_label) is False and (default_label is not None and str(prev_label) != default_label):
                    new_length.append(time_np[i-1] - start_time)

                if pd.isna(label) or (default_label is not None and str(label) == default_label):
                    start_time = time_np[i]
                    prev_label = label
                    continue

                new_time.append(time_np[i])
                new_label.append(label)
                new_confidence.append(0)
                new_comment.append('')

                start_time = new_time[-1]
                prev_label = new_label[-1]


        if len(new_length) < len(new_time):
            new_length.append(time_np[i-1] - new_time[-1])
        

    new_columns = [TIME_HEADER, LABEL_LENGTH_HEADER, LABEL_LABEL_HEADER, LABEL_CONFIDENCE_HEADER, LABEL_COMMENT_HEADER]
    labels_df = pd.DataFrame(list(zip(new_time, new_length, new_label, new_confidence, new_comment)), columns=new_columns)

    return labels_df