import pandas as pd
from pathlib import Path


def search_for_recording_directories(search_dir: Path,data_file_names:str):
    """
    Searches for recording directories by searching recursively for the first data_file_name
    and returning the parent path.
    """
    data_files = list(search_dir.rglob(data_file_names[0]))
    recording_dirs = [p.parents[0] for p in data_files]
    return recording_dirs

def joining_data_files(input_dir:Path, data_file_names:str) :
    path_datafolders = search_for_recording_directories(input_dir,data_file_names)

    if len(path_datafolders) == 0:
        print(f"Could not find any data files with given path: {path_datafolders}")
        return

    for path_datafolder in path_datafolders:

        for join_file_number in range(len(data_file_names)):
            # read first join data file as dataframe
            if join_file_number== 0:
                first_join_file = data_file_names[join_file_number]
                first_join_file_name = first_join_file.split(".")[0]
                df_join = pd.read_csv(path_datafolder/first_join_file,names=['Time (seconds)',first_join_file_name+"0", first_join_file_name+"1", first_join_file_name+"2"],header=0)
            else:
                # read other join data file as dataframe
                join_file = data_file_names[join_file_number]
                join_file_name =join_file.split(".")[0]
                df_2join = pd.read_csv(path_datafolder/join_file,names=['Time (seconds)',join_file_name+"0", join_file_name+"1", join_file_name+"2"],header=0)
                # merge two data join files
                df_join = df_join.set_index('Time (seconds)').join(df_2join.set_index('Time (seconds)'))


        df_join.to_csv(path_datafolder/'join.data')