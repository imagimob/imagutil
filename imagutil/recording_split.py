import math
import shutil
import subprocess

from pathlib import Path

import pandas as pd

from .files import VIDEO_FILENAME, DATA_EXT, IMSESSION_EXT, LABEL_EXT
from .files import search_for_recording_directories
from .files import get_mirrored_output_dir
from .headers import TIME_HEADER as TIME_HEADER_DEFAULT, LABEL_LENGTH_HEADER
from .headers import LABEL_LABEL_HEADER, LABEL_CONFIDENCE_HEADER, LABEL_COMMENT_HEADER


LABEL_CSV_COLUMNS = [
    TIME_HEADER_DEFAULT, LABEL_LENGTH_HEADER, LABEL_LABEL_HEADER, LABEL_CONFIDENCE_HEADER, LABEL_COMMENT_HEADER
]

LABEL_TEMP_END_TIME_HEADER = 'EndTime(Seconds)'

LABEL_OTHER_TAG = 'other'


def split_recordings_with_mirrored_structure(
        root_input_dir,
        root_output_dir,
        recording_length,
        copy_imsession_file=False,
        fill_empty_label_files=True,
        label_crop_size=0.01,
        data_extension=DATA_EXT):
    """
    Create split recordings from an input directory while maintaining the original
    folder structure from the base of root_input_dir.
    """
    recording_dirs = search_for_recording_directories(root_input_dir)
    for recording_dir in recording_dirs:
        output_dir = get_mirrored_output_dir(recording_dir, root_input_dir, root_output_dir)
        split_recording_dir(recording_dir,
                            output_dir,
                            recording_length,
                            copy_imsession_file,
                            fill_empty_label_files,
                            label_crop_size,
                            data_extension)


def split_recording_dir(
        recording_dir,
        output_dir,
        recording_length,
        copy_imsession_file=False,
        fill_empty_label_files=True,
        label_crop_size=0.01,
        data_extension=DATA_EXT):
    """
    Splits recording_dir into chunks of recording_length size and put in
    output_dir. The split recording directories will be named according
    to the following principle: "<original_name>_<split_index>"
    """
    # TODO: handle fill_empty_label_files and label_crop_size
    recording_name = recording_dir.name

    # Split data files
    data_files = list(recording_dir.glob('*' + data_extension))
    for data_file in data_files:
        split_data_file(data_file, output_dir, recording_name, recording_length)

    # Split label files
    label_files = list(recording_dir.glob('*' + LABEL_EXT))
    for label_file in label_files:
        split_label_file(label_file, output_dir, recording_name, recording_length, label_crop_size)

    # Split video file
    video_file = recording_dir / VIDEO_FILENAME
    if video_file.exists():
        split_video_file_with_ffmpeg(video_file, output_dir, recording_name, recording_length)
    else:
        print(f"ERROR: could not find video file at {str(video_file)}")

    split_dirs = list(output_dir.glob(recording_name + '*'))
    split_dirs = [d for d in split_dirs if d.is_dir()]

    # Copy renamed .imsession files
    if copy_imsession_file:
        imsession_file = recording_dir / (recording_name + IMSESSION_EXT)
        if imsession_file.exists():
            copy_imsession_file_to_split_directories(imsession_file, split_dirs)
        else:
            print(f"ERROR: Could not find .imsession file in recording dir: {recording_dir}")

    # Add empty label files where ones do not exist
    label_file_names = [lf.name for lf in label_files]
    add_empty_label_files(split_dirs, label_file_names)

    # Fill empty label files with "other" class
    if fill_empty_label_files:
        fill_empty_label_files_with_label(split_dirs, label_crop_size)


def split_data_file(data_file, output_dir, recording_name, recording_length):
    """
    Split a .data file into multiple files of recording_length size (in seconds).
    """
    data = pd.read_csv(data_file)
    data_filename = data_file.name
    
    if not TIME_HEADER_DEFAULT in data.columns:
        DATA_TIME_HEADER = data.columns[0]
    else:
        DATA_TIME_HEADER = TIME_HEADER_DEFAULT

    split_index = 0
    split_start_time = 0.0
    while True:
        split_end_time = split_start_time + recording_length

        # Get data within start and end time
        data_chunk = data.loc[
            (data[DATA_TIME_HEADER] >= split_start_time) &
            (data[DATA_TIME_HEADER] < split_end_time)].copy()

        if len(data_chunk) == 0:
            print(f"Splitting of file completed: {str(data_file)}")
            break

        # Remove time offset to start at 0
        data_chunk[DATA_TIME_HEADER] = data_chunk[DATA_TIME_HEADER] - split_start_time

        # Create new output directory and write the data chunk
        split_output_dir = get_split_output_directory(output_dir, recording_name, split_index)
        split_output_dir.mkdir(exist_ok=True, parents=True)
        split_output_file = split_output_dir / data_filename

        print(f"Writing split data file: {str(split_output_file)}")
        data_chunk.to_csv(str(split_output_file), index=False)

        split_index += 1
        split_start_time = split_end_time


def split_label_file(label_file, output_dir, recording_name, recording_length, label_crop_size):
    """
    Splits a .label file into multiple .label files of recording_length length in seconds.
    """
    labels_df = pd.read_csv(label_file)
    if not TIME_HEADER_DEFAULT in labels_df.columns:
        LABEL_TIME_HEADER = labels_df.columns[0]
    else:
        LABEL_TIME_HEADER = TIME_HEADER_DEFAULT

    # Make sure labels_df is non-empty
    if len(labels_df) == 0:
        print(f"No labels to split in {label_file}")
        return

    label_filename = label_file.name

    # Set end time of labels to start time + duration
    print(f"label_filename: {label_filename}")
    print(f"labels_df.columns: {labels_df.columns}")
    labels_df[LABEL_TEMP_END_TIME_HEADER] = labels_df[LABEL_TIME_HEADER] + labels_df[LABEL_LENGTH_HEADER]

    # Get the last label end time to know when to stop
    last_label_end_time = labels_df[LABEL_TEMP_END_TIME_HEADER].max()

    split_index = 0
    split_start_time = 0.0
    while split_start_time < last_label_end_time:
        split_end_time = split_start_time + recording_length

        current_labels = get_labels_in_split_interval(labels_df, split_start_time, split_end_time, label_crop_size)

        # Create new output directory and write the split up label file
        split_output_dir = get_split_output_directory(output_dir, recording_name, split_index)
        split_output_dir.mkdir(exist_ok=True, parents=True)
        split_output_file = split_output_dir / label_filename

        print(f"Writing split label file: {str(split_output_file)}")
        current_labels.to_csv(split_output_file, index=False)

        split_index += 1
        split_start_time = split_end_time


def get_labels_in_split_interval(labels_df, split_start_time, split_end_time, label_crop_size):
    """
    Extracts labels in labels_df within the time interval given by [split_start_time, split_end_time]
    and splits them when the label stretches outside of the given time interval.
    """
    current_labels = pd.DataFrame(columns=labels_df.columns)

    if not TIME_HEADER_DEFAULT in labels_df.columns:
        LABEL_TIME_HEADER = labels_df.columns[0]
    else:
        LABEL_TIME_HEADER = TIME_HEADER_DEFAULT

    for row_index, row in labels_df.iterrows():
        # Copy to make sure to not override information
        row = row.copy()
        row_start_time = row[LABEL_TIME_HEADER]
        row_end_time = row[LABEL_TEMP_END_TIME_HEADER]

        # Check if label or part of label is within current split interval
        if time_interval_overlaps(split_start_time, split_end_time,
                                  row_start_time, row_end_time):
            # If the label starts before the current split, set it to start at the current split
            if row_start_time < split_start_time:
                row[LABEL_TIME_HEADER] = split_start_time + label_crop_size

            # If the label ends after the current split, set it to end at the current split
            if row_end_time > split_end_time:
                row[LABEL_TEMP_END_TIME_HEADER] = split_end_time - label_crop_size

            # Re-calculate the length of the current label
            row[LABEL_LENGTH_HEADER] = row[LABEL_TEMP_END_TIME_HEADER] - row[LABEL_TIME_HEADER]

            current_labels = current_labels.append(row)

    # Offset the labels so time starts at 0
    current_labels[LABEL_TIME_HEADER] = current_labels[LABEL_TIME_HEADER] - split_start_time

    # Drop the end time column again
    current_labels = current_labels.drop(columns=[LABEL_TEMP_END_TIME_HEADER])
    current_labels.columns = LABEL_CSV_COLUMNS
    return current_labels


def time_interval_overlaps(start_time_0, end_time_0, start_time_1, end_time_1):
    if start_time_0 <= start_time_1 and end_time_0 >= start_time_1:
        return True
    elif start_time_1 <= start_time_0 and end_time_1 >= start_time_0:
        return True
    else:
        return False


def split_video_file_with_ffmpeg(video_file, output_dir, recording_name, recording_length):
    """
    Split a video file into multiple files of recording_length size (in seconds).
    """
    video_length = get_video_length(str(video_file))
    split_count = int(math.ceil(video_length / float(recording_length)))
    print(f"Splitting video {str(video_file)} of length {video_length} into "
          f"{split_count} parts of length {recording_length}.")

    for split_index in range(0, split_count):
        split_output_dir = get_split_output_directory(output_dir, recording_name, split_index)
        split_output_dir.mkdir(exist_ok=True, parents=True)
        split_output_file = split_output_dir / VIDEO_FILENAME
        split_start = recording_length * split_index
        run_ffmpeg(video_file, split_output_file, split_start, recording_length)


def run_ffmpeg(video_file, output_file, split_start, recording_length):
    cmd = [
        "ffmpeg", "-y", "-i", str(video_file),
        "-vcodec", "copy", "-acodec", "copy",
        "-ss", str(split_start),
        "-t", str(recording_length),
        str(output_file)]

    str_cmd = " ".join(cmd)

    print(f"Running command: {str_cmd}")
    subprocess_output = subprocess.run(cmd, capture_output=True)
    if subprocess_output.returncode != 0:
        print(f"ERROR: failed to generate video with ffmpeg: {str(output_file)}")
        print("Output from stderr:\n" + subprocess_output.stderr.decode("utf-8"))


def get_split_output_directory(output_dir: Path, recording_name: str, split_index: int):
    """
    Get the path of a split recording directory from the original recording_name
    and the split_index.
    """
    return output_dir / f"{recording_name}_{split_index}"


def copy_imsession_file_to_split_directories(imsession_file, split_dirs):
    """
    Copy .imsession files to new directories and renaming them to follow the
    convention expected by Imagimob Studio.
    """
    for split_dir in split_dirs:
        split_name = split_dir.name
        imsession_dst = split_dir / (split_name + IMSESSION_EXT)
        shutil.copy(str(imsession_file), str(imsession_dst))
        print(f"Copied .imsession file to: {str(imsession_dst)}")


def copy_file_to_split_directories(orig_file, split_dirs):
    """
    Copy any file into split directories while maintaining the original file name.
    """
    filename = orig_file.name
    for split_dir in split_dirs:
        file_dst = split_dir / filename
        shutil.copy(str(orig_file), str(file_dst))
        print(f"Copied file to: {str(file_dst)}")


def check_if_ffmpeg_available():
    """
    Checks if ffmpeg is available on the PATH environment variable
    """
    available = True
    try:
        _ = subprocess.check_output('where ffmpeg')
        _ = subprocess.check_output('where ffprobe')
    except subprocess.CalledProcessError:
        available = False
    return available


def get_video_length(filename):
    """
    Checks the length of a video in seconds using ffprobe.
    """
    probe_cmd = (
        "ffprobe", "-v", "error", "-show_entries", "format=duration", "-of",
        "default=noprint_wrappers=1:nokey=1", filename)
    output = subprocess.check_output(probe_cmd).strip()
    video_length = int(float(output))
    return video_length


def add_empty_label_files(src_dirs, label_file_names):
    """
    Check if label file names specified by label_file_names exist in src_dirs otherwise
    add empty label files.
    """
    for src_dir in src_dirs:
        for label_file_name in label_file_names:
            label_file_path = src_dir / label_file_name
            if not label_file_path.exists():
                # Create empty label dataframe and output to file
                print(f"Creating empty label file: {label_file_path}")
                empty_label_df = pd.DataFrame(columns=LABEL_CSV_COLUMNS)
                empty_label_df.to_csv(label_file_path, index=False)


def fill_empty_label_files_with_label(src_dirs, label_crop_size=0.01):
    """
    Search for empty label files in src_dirs and add other label in such files.
    """
    for src_dir in src_dirs:
        label_files = list(src_dir.glob('*' + LABEL_EXT))
        for label_file in label_files:
            label_data = pd.read_csv(label_file)
            if len(label_data) == 0:
                start_time, end_time = get_time_interval_from_data_files(src_dir)
                if not math.isnan(start_time) and not math.isnan(end_time):
                    label_start = start_time + label_crop_size
                    label_length = end_time - start_time - label_crop_size
                    label_row = [label_start, label_length, LABEL_OTHER_TAG, 0, ""]
                    label_data.loc[0] = label_row
                    print(f"Filling empty label file with 'other' label: {label_file}")
                    label_data.to_csv(label_file, index=False)
                else:
                    print(f"ERROR: Could not find .data files in {src_dir}.")


def get_time_interval_from_data_files(src_dir):
    """
    Read all .data files in src_dir and return the time interval for all data.
    """
    data_files = list(src_dir.glob('*' + DATA_EXT))

    if len(data_files) == 0:
        return float('nan'), float('nan')

    data_list = [pd.read_csv(data_file) for data_file in data_files]
    data_df = pd.concat(data_list)

    if not TIME_HEADER_DEFAULT in data_df.columns:
        DATA_TIME_HEADER = data_df.columns[0]
    else:
        DATA_TIME_HEADER = TIME_HEADER_DEFAULT

    start_time = data_df[DATA_TIME_HEADER].min()
    end_time = data_df[DATA_TIME_HEADER].max()

    return start_time, end_time