from .headers import TIME_HEADER, LABEL_LENGTH_HEADER, LABEL_LABEL_HEADER, LABEL_CONFIDENCE_HEADER, LABEL_COMMENT_HEADER
import pandas as pd
import numpy as np
import argparse
import os

DESCRIPTION = """
Convert windows into timestamps with a specified delta time.
"""

def get_timestamps_data_from_windows(data, time_windows, dt):
    """
    Convert a dataframe to a numpy array with timestamps.
    """
    np_data = data.to_numpy()

    time_windows = np.array(time_windows)

    current_time = 0
    converted_data = []
    
    for i in range(len(time_windows)):
        t = time_windows[i]
        x = np_data[i]

        while(current_time < t):
            row = np.concatenate(([current_time], x), axis=0)
            converted_data.append(row)
            current_time += dt

    return np.array(converted_data)

def convert_dataframe_windows_to_timestamps(data_frame, time_column_name, dt):
    """
    Convert windows in a dataframe to timestamps.
    """
    time = data_frame[time_column_name]
    data = data_frame.drop(time_column_name, axis=1)
    np_data = get_timestamps_data_from_windows(data, time, dt)

    columns = [TIME_HEADER] + [data.columns[i] for i in range(data.shape[1])]
    data_frame = pd.DataFrame(data=np_data,
                              index=[i for i in range(np_data.shape[0])],
                              columns=[columns[i] for i in range(data_frame.shape[1])])

    return data_frame