from .headers import TIME_HEADER, LABEL_LENGTH_HEADER, LABEL_LABEL_HEADER, LABEL_CONFIDENCE_HEADER, LABEL_COMMENT_HEADER
from .files import VIDEO_FILENAME, DATA_EXT, IMSESSION_EXT, LABEL_EXT

import pandas as pd
import glob

def add_timestamps_to_data(data, hz):
    """
    Add a time column to a dataframe with a timecolumn with the specified hz.
    """
    c_t = 0     # Current time
    dt = 1/hz
    time = []
    for index, row in data.iterrows():
        time.append(c_t)
        c_t += dt

    data.insert(0, TIME_HEADER, time)

    print('Finished adding timestamps at {} seconds'.format(c_t))
    return data


def add_timestamp_column_to_data_files(root_input_dir, root_output_dir, hz, data_extension=DATA_EXT):
    """
    Add a time column to all data files in the input directory and write them to the output directory.
    """
    for data_file in glob.glob(root_input_dir + '/*/*' + data_extension):
        data = pd.read_csv(data_file)
        data = add_timestamps_to_data(data, hz)

def add_timestamps_to_data_frames(data_frames, hz):
    """
    Add a time column to a list of dataframes with a timecolumn with the specified hz.
    """
    for df in data_frames:
        c_t = 0     # Current time
        dt = 1/hz
        time = []
        for index, row in df.iterrows():
            time.append(c_t)
            c_t += dt

        df.insert(0, TIME_HEADER, time)

    print('Finished adding timestamps in a new column named {}'.format(TIME_HEADER))
    return data_frames
    