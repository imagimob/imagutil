from pathlib import Path
import pandas as pd
import os

VIDEO_FILENAME = 'video.mp4'
DATA_EXT = '.csv'
IMSESSION_EXT = '.imsession'
LABEL_EXT = '.label'


def search_for_recording_directories(search_dir: Path):
    """
    Searches for recording directories by searching recursively for .imsession files
    and returning the parent path.
    """
    imsession_files = list(search_dir.rglob('*' + IMSESSION_EXT))
    recording_dirs = [p.parents[0] for p in imsession_files]
    return recording_dirs

def get_data_frame_list(root_path, has_headers=True, data_extension=DATA_EXT):
    """
    Returns a list of dataframes from a given root path.
    """
    paths = get_data_file_paths(root_path, data_extension=DATA_EXT)

    data_frames = []
    for path in paths:
        print('Loading file {}'.format(path))
        if has_headers:
            data_frames.append(pd.read_csv(path))
        else:
            data_frames.append(pd.read_csv(path, header=None))
    return data_frames

def get_data_file_paths(root_path, data_extension=DATA_EXT):
    """
    Returns a list of paths to .csv files.
    """
    paths = []
    for data_file in Path(root_path).rglob('*' + data_extension):
        paths.append(str(data_file))
    return paths

def getFolderPathsFromFilePaths(input_path):
    """
    Remove file name from paths and return the paths
    """
    output_paths = []
    for path in input_path:
        output_paths.append(os.path.dirname(path))
    return output_paths

def get_file_name_from_path(path):
    """
    Returns the file name from a given path.
    """
    return os.path.basename(path)

def get_output_paths_from_input_paths(input_paths: list) -> list:
    """
    Returns a list of output paths from a given list of input paths.
    """
    folders = getFolderPathsFromFilePaths(input_paths)

    if len(folders) > 1 and folders[0] != folders[1]:
        # If the paths are not in the same folder, use those folders
        return folders

    output_paths = []
    for input_path, folder_path in zip(input_paths, folders):
        # If all data files are in the same folder, create sub folders using the file names
        file_name = get_file_name_from_path(input_path)
        new_folder_name = file_name.split('.')[0]
        output_paths.append(os.path.join(folder_path, new_folder_name))

    return output_paths

def get_extended_output_paths_from_input_paths(input_paths: list, output_path: str) -> list:
    """
    Get extended output paths using data sub folders or file names from input_paths.
    """
    folders = getFolderPathsFromFilePaths(input_paths)

    if len(folders) > 1 and folders[0] != folders[1]:
        return

    output_paths = []
    for input_path, folder_path in zip(input_paths, folders):
        # If all data files are in the same folder, create sub folders using the file names
        file_name = get_file_name_from_path(input_path)
        new_folder_name = file_name.split('.')[0]
        output_paths.append(os.path.join(folder_path, new_folder_name))

    return output_paths

def get_mirrored_output_dir(recording_dir, root_input_dir, root_output_dir):
    """
    Returns a mirrored output_dir structure from a given recording_dir
    and root_input_dir that should be a subset of recording_dir.
    """
    parts_to_keep = recording_dir.parts[len(root_input_dir.parts):]
    mirrored_output_dir = root_output_dir / Path(*parts_to_keep)
    return mirrored_output_dir

def save_data_frame_list(data_frame, labels_data_frame, root_output_path):
    """
    Saves a list of data frames and labels data frames.
    """
    print('Start saving files')
    for i, (data, labels) in enumerate(zip(data_frame, labels_data_frame)):
        output_dir = os.path.join(root_output_path, 'split{}'.format(i))
        output_data = os.path.join(output_dir, 'data.csv')
        output_label = os.path.join(output_dir, 'label.csv')

        if os.path.isdir(output_dir) is False:
            os.mkdir(output_dir)

        data.to_csv(output_data, index=False)
        labels.to_csv(output_label, index=False)
    print('Done saving files')

def get_output_paths_from_input_and_output_path(input_paths: list, output_root_path: str) -> list:
    """
    Returns a list of output paths from a given list of input paths and output root path.
    """
    output_paths = []
    for input_path in input_paths:
        file_name = get_file_name_from_path(input_path)
        new_folder_name = file_name.split('.')[0]
        output_paths.append(os.path.join(output_root_path, new_folder_name))
    return output_paths

def rename_files(root_path, original_name, new_name):
    """
    Renames files in a given directory.
    """
    print('Start renaming files')
    for file in root_path.rglob(original_name):
        print('Changing name of {} to {}'.format(file, new_name))
        os.rename(file, file.parent / new_name)

if __name__ == '__main__':
    path1 = 'unit-tests/data/path1/'
    path2 = 'unit-tests/data/path2/'
    test_1 = get_data_frame_list(path1)
    test_2 = get_data_frame_list(path2)
    kalle = 0