from imagutil.files import LABEL_EXT
import argparse

from pathlib import Path
from imagutil.EmptyLabelTrack import add_empty_label_track

DESCRIPTION = \
"""
Automatically add an empty label track for all data in the diretory.

Algorithm parameter descriptions:

input_dir: the directory where you save your data.

track_name: the name for the empty label track, the default name is "empty.label"
"""

def main():
    args = parse_args()
    input_dir = Path(args.input_dir)
    label_track_name = args.label_track_name + LABEL_EXT
    add_empty_label_track(input_dir, label_track_name)

def parse_args():
    parser = argparse.ArgumentParser(DESCRIPTION)
    parser.add_argument(
        '--input-dir',
        required=True,
        type=str,
        help='Root directory to recursively search for data files in.')
    parser.add_argument(
        '--label-track-name',
        type=str,
        default="empty",
        help='The desired name to set for the empty label track.')
    return parser.parse_args()

if __name__ == "__main__":
    main()