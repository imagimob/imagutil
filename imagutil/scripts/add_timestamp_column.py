import argparse

from imagutil.timestamps import add_timestamp_column_to_data_files

DESCRIPTION = \
"""
Add timestamp column to file
"""

def main():
    args = parse_args()
    add_timestamp_column_to_data_files(args.input_dir, '', 10)
    return 0

def parse_args():
    """
    Parse arguments from command line.
    """
    parser = argparse.ArgumentParser(DESCRIPTION)
    parser.add_argument(
        '-i', '--input-dir',
        required=True,
        type=str,
        help='Root directory to recursively search for recordings in.')

    return parser.parse_args()

if __name__ == '__main__':
    main()