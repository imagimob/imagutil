import argparse

import numpy as np
import pandas as pd


DESCRIPTION = """
Convert timestamps in csv formatted file to Imagimob timestamps format.
"""

STUDIO_TIMESTAMP_COLUMN = 'Time(Seconds)'

# # Conversion functions go here # #
# To add more support for more conversion functions, simply create a function
# here and add it to the CONVERSION_FUNCTIONS dictionary.
# The input and output should be a pandas DataFrame object.
# Make sure the timestamps column is inserted as the first column.


def convert_stbox_timestamps(input_data: pd.DataFrame) -> pd.DataFrame:
    time_of_day_column = "hh:mm:ss.ms"
    date_column = "dd/mm/yyyy"

    # Conversion through datetime representation to nanoseconds to seconds as float
    series_merged_date = input_data[date_column] + ' ' + input_data[time_of_day_column]
    series_datetime = pd.to_datetime(series_merged_date, dayfirst=True)
    series_nanoseconds = pd.to_numeric(series_datetime)
    series_seconds = series_nanoseconds.astype(np.float64) / np.float64(1e9)

    new_data = input_data.drop(columns=[time_of_day_column, date_column])
    new_data.insert(loc=0, column=STUDIO_TIMESTAMP_COLUMN, value=series_seconds)

    return new_data

# # # # # # # # # # # # # # # # # # #


CONVERSION_FUNCTIONS = {
    'stbox': convert_stbox_timestamps
}


def parse_args():
    parser = argparse.ArgumentParser(DESCRIPTION)

    parser.add_argument(
        "-i", "--input",
        dest="input",
        type=str,
        required=True,
        help="Path to input file to convert.")

    parser.add_argument(
        "-o", "--output",
        dest="output",
        type=str,
        required=True,
        help="Path to output file.")

    parser.add_argument(
        "-f", "--format",
        dest="format",
        type=str,
        required=True,
        choices=CONVERSION_FUNCTIONS.keys(),
        help="Format standard of input timestamps.")

    return parser.parse_args()


def main():
    args = parse_args()

    if args.format not in CONVERSION_FUNCTIONS:
        # Technically this could not happen when using choices in argparse
        raise SystemExit(f"Unsupported format {args.format}")

    input_data = pd.read_csv(args.input)

    conversion_func = CONVERSION_FUNCTIONS[args.format]
    output_data = conversion_func(input_data)

    print(f"Writing to file: {args.output}")
    output_data.to_csv(args.output, index=False)


if __name__ == '__main__':
    main()
