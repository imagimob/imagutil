import argparse
from pathlib import Path

from imagutil.files import DATA_EXT
from imagutil.auto_labeling import generate_labels_recursively, ThresholdStrategy, DataProcessStrategy

DESCRIPTION = \
"""
Automatically generate labels for multi-dimentional signal data using the following approach:
- Process all feature values to get 1D feature signal
- Apply hanning window smoothing
- Threshold data to binary signal
- Apply binary opening to remove noise in binary signal
- Apply binary closing to close gaps in binary signal
- Convert continous sequences of ones in binary signal to labels

Algorithm parameter descriptions:
features_process_strategy:
  Can either be "mean" or "sum". Selecting "mean" will comput the average
  of all feature value or "sum" will sum up all the feature value to get 1d
  feature signal for next step.
threshold_strategy:
  Can either be "percentage" or "value". Selecting "value" will simply define
  the threshold as a raw signal amplitude value. E.g. if the range of your
  data is [0, 300], then your threshold should be specified in that range.
  If you select "percentage" then your threshold should be in the range of
  [0.0, 1.0] and is specified as a ratio on the min/max range of your input
  data.
threshold_value:
  Simply the value of the threshold to apply to the data. This will highly
  depend on the input data. As a default the script will use a threshold
  value of 0.5 together with threshold strategy "percentage". This can be
  a good starting point value but will likely need to be adjusted depending
  on the data characteristics.
hanning_len:
  Defines the length of the hanning window used for smoothing, a longer
  window length will result in a smoother signal before the thresholding
  step. E.g. for 50 Hz data a hanning length of ~11 is reasonable, but
  it depends on how noisy the input signal is.
opening_steps:
  Defines the number of iterations to use for the binary opening operation.
  Opening is most useful if the signal is noisy and cannot be smoothed to
  a reasonable extent. Applying 5 iterations of opening will remove "noise"
  which has a length of 5 in number of samples. E.g. if we consider labels
  of length 0.1 seconds as noise, given 100 Hz sampling frequency we would
  want to apply 0.1 * 100 = 10 opening steps to remove such noise.
closing_steps:
  Defines the number of iterations to use for the binary closing operation.
  The closing operation will close gaps in the binary signal. So if your
  generated labels contain gaps, you can increase the closing steps to
  close such gaps. E.g. if we have gaps of length 0.5 seconds in our
  labels, given 100 Hz sampling frequency, we would want to apply
  0.5 * 100 = 50 closing steps to close such gaps.
"""

THRESHOLD_STRATEGY_DEFAULT = ThresholdStrategy.percentage
DATA_STRATEGY_DEFAULT = DataProcessStrategy.mean
THRESHOLD_VALUE_DEFAULT = 0.5

def main():
    args = parse_args()
    input_dir = Path(args.input_dir)
    label = args.label
    label_file_name = args.label_file_name
    data_process_strategy = DataProcessStrategy[args.data_process_strategy]
    data_file_wildcard = args.data_file_name
    threshold_value = args.threshold_value
    threshold_strategy = ThresholdStrategy[args.threshold_strategy]
    hanning_length = args.hanning_length
    opening_steps = args.opening_steps
    closing_steps = args.closing_steps

    generate_labels_recursively(
        input_dir, label, label_file_name, data_process_strategy, data_file_wildcard,
        threshold_value, threshold_strategy, hanning_length,
        opening_steps, closing_steps)


def parse_args():
    parser = argparse.ArgumentParser(DESCRIPTION)
    parser.add_argument(
        '--input-dir',
        required=True,
        type=str,
        help='Root directory to recursively search for data files in.')
    parser.add_argument(
        '--label',
        type=str,
        required=True,
        help='The desired label name to set for the labels.')
    parser.add_argument(
        '--label-file-name',
        type=str,
        default='labels',
        help='The desired file name for the newly created label file {file-name}.label')
    parser.add_argument(
        '--data-process-strategy',
        type = str,
        default=DATA_STRATEGY_DEFAULT.name,
        choices=DataProcessStrategy.list_names(),
        help = 'The desired processing strategy for multi dimentional data'
              'choose either "mean" for computing the average of all features or "sum" for the sum of all features value ')
    parser.add_argument(
        '--data-file-name',
        type=str,
        default="*" + DATA_EXT,
        help='The name of the data file to base labelling on (wildcards allowed). By default searches '
             'for any data file with the .data extension')
    parser.add_argument(
        '--threshold-strategy',
        type=str,
        default=THRESHOLD_STRATEGY_DEFAULT.name,
        choices=ThresholdStrategy.list_names(),
        help='The desired thresholding strategy. Choose either "percentage" for thresholding based on a '
             'percentage of the max signal value or "value" for thresholding directly on raw signal value.')
    parser.add_argument(
        '--threshold-value',
        type=float,
        default=THRESHOLD_VALUE_DEFAULT,
        help='The desired threshold value for auto labelling algorithm. Defaults to 0.5 of signal max value.')
    parser.add_argument(
        '--hanning-length',
        type=int,
        default=0,
        help='Length of Hanning window for applying smoothing to signal before thresholding operation. '
             'If 0 then no smoothing is applied. Defaults to 0.')
    parser.add_argument(
        '--opening-steps',
        type=int,
        default=0,
        help='Binary opening steps to apply after thresholding operation. See description above for '
             'more information. If 0 then no opening is applied. Defaults to 0.')
    parser.add_argument(
        '--closing-steps',
        type=int,
        default=0,
        help='Binary closing steps to apply after opening operation. See description above for '
             'more information. If 0 then no opening is applied. Defaults to 0.')
    return parser.parse_args()


if __name__ == "__main__":
    main()