# from imagutil.labels import convert_row_labels_to_imagimob_labels
from imagutil.labels import convert_row_labels_to_imagimob_labels, convert_row_based_labels_to_imagimob_labels

from imagutil.timestamps import add_timestamps_to_data

import pandas as pd
import numpy as np
import argparse
import os

DESCRIPTION = 'Convert one label per row to imagimob label format.'

TIME_HEADER = 'Time(Seconds)'
LABEL_LENGTH_HEADER = 'Length(Seconds)'
LABEL_LABEL_HEADER = 'Label(string)'
LABEL_CONFIDENCE_HEADER = 'Confidence(double)'
LABEL_COMMENT_HEADER = 'Comment(string)'

DATA_FILE_NAME = 'data_updated.csv'
LABEL_FILE_NAME = 'label.csv'



# Remove file name from path and return that path
def getOutputPathFromFilePath(file_path):
    path = file_path.replace('\\', '/').split('/')
    file_name_length = len(path[-1])
    return file_path[:-file_name_length]

def save_files(data_df, labels_df, output_path):
    data_output_file = os.path.join(output_path, DATA_FILE_NAME)
    label_output_file = os.path.join(output_path, LABEL_FILE_NAME)
    data_output_file = data_output_file.replace('\\', '/')
    label_output_file = label_output_file.replace('\\', '/')

    if not os.path.exists(output_path):
        os.makedirs(output_path)

    data_df.to_csv(data_output_file, index=None)
    labels_df.to_csv(label_output_file, index=None)

def main():
    args = parse_args()
    file_path = args.input_file
    output_path = args.output_path
    label_column_index = args.label_column_index
    label_column_name = args.label_column_name
    time_column_name = args.time_column_name
    hz = args.hz

    df = pd.read_csv(file_path)

    # Get label column name from index
    if label_column_name is None:
        label_column_name = df.columns[label_column_index]
    # If output path is none then save it in same directory as the input data
    if output_path is None:
        output_path = getOutputPathFromFilePath(file_path)

    if hz is not None:
        df = add_timestamps_to_data(df, hz)
        time_column_name = TIME_HEADER

    columns = df.columns

    labels_df = convert_row_based_labels_to_imagimob_labels(df, label_column_name, time_column_name)
    data_df = df.drop(label_column_name, axis=1)

    save_files(data_df, labels_df, output_path)

    return 0

def parse_args():
    parser = argparse.ArgumentParser(DESCRIPTION)
    parser.add_argument(
        '--input-file',
        required=True,
        type=str,
        help='Path to file to be converted.')
    parser.add_argument(
        '--output-path',
        required=False,
        type=str,
        help='Path to directory to save updated data and labels.')
    parser.add_argument(
        '--label-column-index',
        required=False,
        default=-1,
        type=int,
        help='Index of the label column.')
    parser.add_argument(
        '--label-column-name',
        required=False,
        type=str,
        help='Name of the label column, will overwrite label-column-index.')
    parser.add_argument(
        '--time-column-name',
        required=False,
        default='Time [s]',
        type=str,
        help='Name of the time column.')
    parser.add_argument(
        '--hz',
        required=False,
        type=int,
        help='Add timestamps with the given frequency.')
    return parser.parse_args()

if __name__ == '__main__':
    main()