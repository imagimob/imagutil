from imagutil.headers import addHeadersToFiles
import argparse

DESCRIPTION = \
"""
Add headers to data files.
"""

def parse_args():
    parser = argparse.ArgumentParser(DESCRIPTION)
    parser.add_argument(
        '--input-dir',
        required=True,
        type=str,
        help='Root directory to recursively search for data files in.')
    parser.add_argument(
        '--output-dir',
        required=False,
        type=str,
        help='Path to directory to save updated data.')
    parser.add_argument(
        '--file-name',
        required=False,
        type=str,
        help='Name of output file.')
    parser.add_argument(
        '--timestamp-column-index',
        required=False,
        type=int,
        help='Path to directory to save updated data.')
    return parser.parse_args()

def main():
    args = parse_args()
    root_input_path = args.input_dir
    root_output_path = args.output_dir
    file_name = args.file_name
    timestamp_column_index = args.timestamp_column_index
    addHeadersToFiles(root_input_path, root_output_path, timestamp_column_index, file_name)
    return 0

if __name__ == '__main__':
    main()