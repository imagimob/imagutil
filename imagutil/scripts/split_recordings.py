import argparse
import sys

from pathlib import Path

from imagutil.recording_split import split_recordings_with_mirrored_structure
from imagutil.recording_split import check_if_ffmpeg_available

from imagutil.files import DATA_EXT


DESCRIPTION = \
"""
Split recordings into chunks of fixed length. This script will recursively search
for .imsession files in the input directory and split the recordings into chunks
of a given size.

By default labels are cropped by 0.01 seconds in the beginning and end of the new
recordings. This is to avoid issues when opening them in Studio. This value can be
changed using the --label-crop-size flag.
"""

FFMPEG_NOTICE = \
"""
This script requires ffmpeg to be installed and on the $PATH environment variable.
To install ffmpeg please visit:
https://www.ffmpeg.org/download.html
"""


def main():
    """
    Script entrypoint function. See DESCRIPTION for more information.
    """
    args = parse_args()
    input_dir = Path(args.input_dir)
    output_dir = Path(args.output_dir)
    recording_length = float(args.length)
    fill_empty_label_files = args.fill_empty_label_files
    label_crop_size = args.label_crop_size
    copy_imsession_file = args.copy_imsession_file
    data_extension = args.data_extension

    if not check_if_ffmpeg_available():
        print("ERROR: Could not find ffmpeg on path.")
        print(FFMPEG_NOTICE)
        sys.exit(1)

    print(f"Splitting recordings in {str(input_dir)} to {recording_length} second parts")
    split_recordings_with_mirrored_structure(
        input_dir,
        output_dir,
        recording_length,
        copy_imsession_file,
        fill_empty_label_files,
        label_crop_size,
        data_extension)


def parse_args():
    """
    Parse arguments from command line.
    """
    parser = argparse.ArgumentParser(DESCRIPTION + '\n' + FFMPEG_NOTICE)
    parser.add_argument(
        '-i', '--input-dir',
        required=True,
        type=str,
        help='Root directory to recursively search for recordings in.')
    parser.add_argument(
        '-o', '--output-dir',
        required=True,
        type=str,
        help='Directory to write output to.')
    parser.add_argument(
        '-l', '--length',
        type=float,
        required=True,
        help='The desired length in seconds of the new recordings.')
    parser.add_argument(
        '-f', '--fill-empty-label-files',
        action='store_true',
        help="If enabled the splitting tool will fill empty label files with 'other' label.")
    parser.add_argument(
        '-ls', '--label-crop-size',
        type=float,
        default=0.01,
        help="Crop labels to make sure they do extend longer than the data.")
    parser.add_argument(
        '-c', '--copy-imsession-file',
        action='store_true',
        help="Copy original .imsession file to new split recording directories. "
             "It is recommended to create new .imsession files with batch import in Studio.")

    parser.add_argument(
        '-dx', '--data-extension',
        default=DATA_EXT,
        type=str,
        help="Set the file extension for the data files to be split.")
    
    return parser.parse_args()


if __name__ == "__main__":
    main()
