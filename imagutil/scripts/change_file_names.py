import argparse
from pathlib import Path
from imagutil.files import rename_files

DESCRIPTION = \
"""
This script renames files in a directory.
"""

def main():
    args = parse_args()
    rename_files(Path(args.root_input_dir), args.file_name, args.new_file_name)
    return 0

def parse_args():
    parser = argparse.ArgumentParser(DESCRIPTION)
    parser.add_argument(
        '--root-input-dir',
        required=True,
        type=str,
        help='Root directory to recursively search for files in.')
    parser.add_argument(
        '--file-name',
        required=True,
        type=str,
        help='Name of files to rename.')
    parser.add_argument(
        '--new-file-name',
        required=True,
        type=str,
        help='Name of new files.')
    return parser.parse_args()

if __name__ == '__main__':
    main()