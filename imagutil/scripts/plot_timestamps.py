import argparse
from pathlib import Path

import pandas as pd
import matplotlib.pyplot as plt


def parse_args():
    parser = argparse.ArgumentParser("Plot histogram of timestamps for .data file.")
    parser.add_argument(
        "-d", "--data",
        dest="data",
        type=str,
        required=True,
        help="Path to input .data files.")
    parser.add_argument(
        "--bins",
        type=int,
        default=None,
        help="Number of bins to use in histogram plot.")
    return parser.parse_args()


def main():
    args = parse_args()
    data_file = Path(args.data)

    data_df = pd.read_csv(data_file)

    timestamp_diff_seconds = data_df["Time (seconds)"].diff().dropna()
    timestamp_diff_ms = (timestamp_diff_seconds * 1000)

    print("Value counts:")
    timestamp_value_counts = timestamp_diff_ms.value_counts(sort=False)
    print(timestamp_value_counts)

    print("Average time difference in ms:")
    print(timestamp_diff_ms.mean())

    timestamp_diff_ms.hist(bins=args.bins)

    plt.title("Timestamp difference histogram")
    plt.ylabel("No. samples")
    plt.xlabel("Difference in milliseconds")
    plt.show()


if __name__ == '__main__':
    main()
