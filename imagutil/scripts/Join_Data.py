from imagutil.files import DATA_EXT
import argparse

from pathlib import Path
from imagutil.join_data import joining_data_files

DESCRIPTION = \
"""
Automatically join data for files name provided by user in the diretory provided.

Algorithm parameter descriptions:

input_dir: the directory where you save your data.

data_file_names: the names for the data files that will be joined and they should be saved 
in the same directory.

example: "--input-dir","input_path","--data-file-names", "gyro.data","accel.data"
"""

def main():
    args = parse_args()
    input_dir = Path(args.input_dir)
    data_file_names = args.data_file_names[0]
    joining_data_files(input_dir, data_file_names)


def parse_args():
    parser = argparse.ArgumentParser(DESCRIPTION)
    parser.add_argument(
        '--input-dir',
        required=True,
        type=str,
        help='Root directory to recursively search for data files in.')
    parser.add_argument(
        '--data-file-names',
        dest='data_file_names',
        type=str,
        required=True,
        action='append', 
        nargs='+',
        help='The names for data files that will be to joined, should be more than 2 names')
    return parser.parse_args()

if __name__ == "__main__":
    main()