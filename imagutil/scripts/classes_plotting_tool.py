"""
Script to plot classes for comparison
"""

import argparse
from pathlib import Path
import os

import matplotlib.pyplot as plt

import numpy as np
import pandas as pd

import xml.etree.ElementTree as ET


DATA_EXT = '.data'
LABEL_EXT = '.label'
IMSESSION_EXT = '.imsession'

DATA_TIME_HEADER = 'Time (seconds)'

# Header of .label file supported by Imagimob Studio
TIME_COLUMN_INDEX = 0  # Time(Seconds)
LENGTH_COLUMN_INDEX = 1  # Length(Seconds)
LABELS_COLUMN_INDEX = 2 # 'Label(string)'

EXTRA_ROWS = 1  # used in finding the start index and end index of a class




def parse_args():
    parser = argparse.ArgumentParser(
        "Classes Plotting Tool. It creates a grid plot with the first (number_of_rows)x(number_of_columns) labels found in each data file, assuming that each data file contains only 1 class.")

    parser.add_argument(
        "--input-dir", "-i",
        required=True,
        type=str,
        help='Root directory to recursively search for data and label files in.')
    
    parser.add_argument(
        "--number-of-rows", "-r",
        default=2,
        type=int,
        help="Specify the number of rows of the grid plot. Default is 2.")

    parser.add_argument(
        "--number-of-columns", "-c",
        default=2,
        type=int,
        help="Specify the number of columns of the grid plot. Default is 2.")
    
    parser.add_argument(                   
        "--output-dir", "-o",
        default='classes_plots',
        type=str,
        help="Directory where to place the pictures of the created grid plots."
             "The default folder is classes_plots")
    
    parser.add_argument(
        '--data-file-name',
        default="*" + DATA_EXT,
        type=str,
        help='The name of the data file to be parsed (wildcards allowed). By default searches '
             'for any data file with the .data extension.')

    parser.add_argument(
        '--label-file-name',
        default="*" + LABEL_EXT,
        type=str,
        help='The name of the label file to be parsed (wildcards allowed). By default searches '
             'for any label file with the .label extension.')

    parser.add_argument(
        '--imsession-file-name',
        default="*" + IMSESSION_EXT,
        type=str,
        help='The name of the IM Session file to be parsed (wildcards allowed). By default searches '
             'for any IM Session file with the .imsession extension.')

    parser.add_argument(
        "--start-column-index", "-sc",
        default=None,
        type=int,
        help="Specify the start column index to be used to slice the data. Default is None.")

    parser.add_argument(
        "--end-column-index", "-ec",
        default=None,
        type=int,
        help="Specify the end column index to be used to slice the data. Default is None.")

    parser.add_argument(
        "--preprocessor",
        action='store_true',
        help='To use the classes plotting tool on Imagimob Studio preprocessor data output.')

    return parser.parse_args()



def search_for_data_files(search_dir: Path, data_file_wildcard: str):
    """
    Searches recursively for files named according to data_file_wildcard
    """
    data_files = list(search_dir.rglob(data_file_wildcard))
    if not data_files:
        print(f"Warning: Could not find any data files named: {data_file_wildcard}")
    
    return data_files





def main():
    args = parse_args()
    number_of_rows = args.number_of_rows
    number_of_columns = args.number_of_columns
    root_input_dir = Path(args.input_dir)
    root_output_dir = Path(args.output_dir)
    data_file_wildcard = args.data_file_name
    label_file_wildcard = args.label_file_name
    
    preprocessor_output = args.preprocessor
    imsession_file_wildcard = args.imsession_file_name

    start_column_index = args.start_column_index
    end_column_index = args.end_column_index

    if start_column_index is not None and end_column_index is not None and start_column_index >= end_column_index:
        print("Start column index must be smaller than end column index")
        return

    columns_range = slice(start_column_index, end_column_index)

    
    if root_output_dir == Path('classes_plots') and not preprocessor_output:    # setting root input folder as a default
        root_output_dir = root_input_dir / ('classes_plots')
    elif root_output_dir == Path('classes_plots') and preprocessor_output:
        root_output_dir = root_input_dir / ('classes_plots_preprocessor')
        data_file_wildcard = "*" + ".csv"
    elif root_output_dir is not Path('classes_plots'):
        root_output_dir = root_input_dir / root_output_dir

    if not os.path.isdir(root_output_dir):
        os.makedirs(root_output_dir)

    print("\n")
    print("Number of Rows of Grid Plot: ", number_of_rows)
    print("Number of Columns  of Grid Plot: ", number_of_columns)
    
    print("Root Input Path: ", root_input_dir)
    print("Root Output Path: ", root_output_dir)
    print("Data Wild Card: ", data_file_wildcard)
    print("Label Wild Card: ", label_file_wildcard)
    print("\n")

    
    data_files = search_for_data_files(root_input_dir, data_file_wildcard)
    
    if len(data_files) == 0:
        print(f"Could not find any data files with given path and wild card: {root_input_dir / data_file_wildcard}")
        return

    if not preprocessor_output:  # data and labels in same directory
        label_files = search_for_data_files(root_input_dir, label_file_wildcard)
    else:   # extracting labels root directory from IM Session files when using Imagimob Studio preprocessor data output
        imsession_files = search_for_data_files(root_input_dir, imsession_file_wildcard)
        label_files = []
        for imsession_file in imsession_files:
            print("IM Session File:", imsession_file)
            tree = ET.parse(imsession_file)
            root = tree.getroot()
            #print(root.tag)
            for track in root.iter("Track"):
                track_type = track.get('type')
                #print(track_type)
                if track_type == 'labelcsv':
                    for payload in track.iter('PayloadFile'):
                        #print(payload.text)
                        #print("Current working directory: {0}".format(os.getcwd()))
                        os.chdir(imsession_file.parents[0]) # Changing working directory to one where IM Session is located
                        #print("Current working directory: {0}".format(os.getcwd()))
                        #path_2 = Path(payload.text) #.parents[0]
                        #print(path_2)
                        path = os.path.abspath(Path(payload.text))
                        #print(path)
                        label_files.append(path)
                        
        
    
    if len(label_files) == 0:
        print(f"Could not find any label files with given path and wild card: {root_input_dir / label_file_wildcard}")
        return


    for data_file, label_file in zip(data_files, label_files):
        print("Data File:", data_file)
        data_df = pd.read_csv(data_file)

        print("Label File:", label_file)
        label_df = pd.read_csv(label_file)
        
        data_rows = data_df.shape[0]
        label_rows = label_df.shape[0]

        number_of_features = data_df.shape[1] - 1   # removing time column

        if start_column_index is not None and start_column_index >= number_of_features and end_column_index is not None and end_column_index >= number_of_features:
            print("Start column index and end column index out of bounds.")
            return
        elif start_column_index is not None and start_column_index < number_of_features and end_column_index is not None and end_column_index > number_of_features:
            print("End column index out of bounds.")
            return

        labels_time_indices = []

        data_row = 0
        for row in range(label_rows):
            start_label_time = label_df.iloc[row, TIME_COLUMN_INDEX]
            end_label_time = start_label_time + label_df.iloc[row, LENGTH_COLUMN_INDEX]
            class_name = label_df.iloc[row, LABELS_COLUMN_INDEX]

            # extracting start index and end index of a class
            while data_row < (data_rows - 1):
                if data_df.iloc[data_row, 0] < start_label_time and data_df.iloc[data_row + EXTRA_ROWS, 0] > start_label_time:
                    class_start_time_index = data_row
                    while data_row < (data_rows - 1):
                        if data_df.iloc[data_row, 0] > end_label_time:
                            class_end_time_index = data_row + EXTRA_ROWS
                            break
                        else:
                            data_row += 1
                    break
                else:
                    data_row += 1

            # appending found classes to a list
            labels_time_indices.append((class_name, class_start_time_index, class_end_time_index))



        # Creating grid plot
        total_plots = number_of_rows*number_of_columns

        labels_time_indices_slice = labels_time_indices[0:total_plots]

        data_df.set_index(DATA_TIME_HEADER, inplace=True)

        plot_y_label = data_df.iloc[:,columns_range].columns.to_list()

        plot_features = ''
        for label in plot_y_label:
            plot_features += f'{label}' + '  '
        #print(plot_features)

        if number_of_rows > 1 and number_of_columns > 1:  # Plotting grid with more than 1 row and 1 column
            fig, axs = plt.subplots(number_of_rows, number_of_columns)
            fig.suptitle('Class Name: ' + f'{labels_time_indices_slice[0][0]}')
            #fig.supylabel(plot_features)

            plot_number = 0
            for row in range(number_of_rows):
                for column in range(number_of_columns):
                    if plot_number < total_plots and plot_number < label_rows:
                        axs[row, column].plot(data_df.iloc[labels_time_indices_slice[plot_number][1]:labels_time_indices_slice[plot_number][2], columns_range])
                        if column == 0 and row == (number_of_rows - 1): # Setting x labels only on plots on the last row of the grid
                            axs[row, column].set(xlabel='Time (seconds)')
                        elif row == (number_of_rows - 1) and column > 0:
                            axs[row, column].set(xlabel='Time (seconds)')
                        #elif column == 0 and row < (number_of_rows - 1): # Setting y labels only on plots on the first column of the grid
                        #    axs[row, column].set(ylabel='features')
                        plot_number += 1

            fig.legend(plot_y_label, title='Features', title_fontsize=10, loc='center right', bbox_to_anchor=(1.01, 0.5))
            fig.tight_layout()
            fig.subplots_adjust(right=0.75)

        elif number_of_rows == 1 and number_of_columns == 2:  # Plotting grid with 1 row and 2 columns
            fig, (ax1, ax2) = plt.subplots(1, 2)
            ax1.plot(data_df.iloc[labels_time_indices_slice[0][1]:labels_time_indices_slice[0][2], columns_range])
            ax1.set(xlabel='Time (seconds)')
            ax2.plot(data_df.iloc[labels_time_indices_slice[1][1]:labels_time_indices_slice[1][2], columns_range])
            ax2.set(xlabel='Time (seconds)')
            fig.suptitle('Class Name: ' + f'{labels_time_indices_slice[0][0]}')
            #fig.supylabel(plot_features)
            fig.legend(plot_y_label, title='Features', title_fontsize=10, loc='center right', bbox_to_anchor=(1.01, 0.5))
            fig.tight_layout()
            fig.subplots_adjust(right=0.75)
        elif number_of_rows == 2 and number_of_columns == 1:  # Plotting grid with 2 rows and 1 column
            fig, (ax1, ax2) = plt.subplots(2)
            ax1.plot(data_df.iloc[labels_time_indices_slice[0][1]:labels_time_indices_slice[0][2], columns_range])
            #ax1.set(ylabel=f'{plot_y_label}')
            ax2.plot(data_df.iloc[labels_time_indices_slice[1][1]:labels_time_indices_slice[1][2], columns_range])
            ax2.set(xlabel='Time (seconds)')
            fig.suptitle('Class Name: ' + f'{labels_time_indices_slice[0][0]}')
            #fig.supylabel(plot_features)
            fig.legend(plot_y_label, title='Features', title_fontsize=10, loc='center right', bbox_to_anchor=(1.01, 0.5))
            fig.tight_layout()
            fig.subplots_adjust(right=0.75)
        elif number_of_rows == 1 and number_of_columns == 1:   # Plotting grid with 1 row and 1 column
            fig, ax = plt.subplots()
            ax.plot(data_df.iloc[labels_time_indices_slice[0][1]:labels_time_indices_slice[0][2], columns_range])
            ax.set(xlabel='Time (seconds)')
            fig.suptitle('Class Name: ' + f'{labels_time_indices_slice[0][0]}')
            #fig.supylabel(plot_features)
            fig.legend(plot_y_label, title='Features', title_fontsize=10, loc='center right', bbox_to_anchor=(1.01, 0.5))
            fig.tight_layout()
            fig.subplots_adjust(right=0.75)
        
        
        # Saving output Data Frame to file
        output_class_dir = root_output_dir / (f'{class_name}')

        if not os.path.isdir(output_class_dir):
            os.makedirs(output_class_dir)

        output_file_path = output_class_dir / ('grid_plot_' + data_file.parent.name + '.png')
        plt.savefig(output_file_path, dpi = 300)
        
    print('-----------------------------------------------')
    print('Classes Plots Created !\n')
    print('-----------------------------------------------')


if __name__ == "__main__":
    main()