#from xmlrpc.client import Boolean
from imagutil.labels import convert_row_based_labels_to_imagimob_labels
from imagutil.timestamps import add_timestamps_to_data_frames
from imagutil.headers import addHeadersToDataFrames, TIME_HEADER
from imagutil.files import get_data_frame_list, get_output_paths_from_input_paths, get_output_paths_from_input_paths, get_data_file_paths, get_output_paths_from_input_and_output_path, DATA_EXT, LABEL_EXT
from pathlib import Path
import argparse
import glob
import os

DESCRIPTION = 'Convert .csv data into imagimob studio data and label format.\n' \
                'Will recursively search folders for .csv files.\n' \
                'Required arguments: --input-path, --output-data-file-name, --output-label-file-name, --has-headers \n' \
                'If no output path is given the output path will be the same as the input path.\n' \
                'When files has no headers then use \'--has-headers False\' otherwise use \'--has-headers True\'.\n' \
                'If no label column name or label column index is given then the label column will be the last column.\n' \
                '--hz specifies that timestamps will be added to the data under the column name \'TIME_HEADER\'.\n' \
                '--none-label specifies which label will be converted to the \'None\' label. \n' \

def addFoldersToPaths(paths, folder_name):
    new_paths = []
    for i in range(len(paths)):
        path_folder_name = folder_name + '_' + str(i)
        path = os.path.join(paths[i], path_folder_name)
        new_paths.append(path)
        
    return new_paths

# Verify so output paths are not the same
def verifyOutputPaths(output_paths):
    if len(output_paths) <= 1:
        return output_paths
    
    if output_paths[0] != output_paths[1]:
        return output_paths
        
    folder_name = 'dir'
    return addFoldersToPaths(output_paths, folder_name)

# Remove file name from path and return that path
def getOutputPathFromFilePath(input_path):
    """
    Remove file name from paths and return the paths
    """
    output_paths = []
    for path in Path(input_path).rglob('*' + DATA_EXT):
        path = str(path).replace('\\', '/')
        path = str(path).split('/')[:-1]
        path[0] = path[0] + os.path.sep
        path = os.path.join(*path)
        output_paths.append(path)
    return output_paths

# Convert string to bool
def str2bool(str):
  return str.lower() in ('yes', 'true', 'y', 't', '1')

# Save data and label files
def save_files(data_df, labels_df, output_path, data_file_name, label_file_name):
    data_output_file = os.path.join(output_path, data_file_name + DATA_EXT)
    label_output_file = os.path.join(output_path, label_file_name + LABEL_EXT)
    data_output_file = data_output_file.replace('\\', '/')
    label_output_file = label_output_file.replace('\\', '/')

    if not os.path.exists(output_path):
        os.makedirs(output_path)

    data_df.to_csv(data_output_file, index=None)
    labels_df.to_csv(label_output_file, index=None)
    print('Saved data and labels to: ' + output_path)

def getSubDirsFromPaths(paths):
    sub_dirs = []
    for path in paths:
        sub_dirs.append(path.replace('\\', '/').split('/')[-2])
    return sub_dirs

def mergeOutputPathAndSubDirs(output_path, sub_dirs):
    output_paths = []
    for sub_dir in sub_dirs:
        output_paths.append(os.path.join(output_path, sub_dir))
    return output_paths

def createOutputPaths(input_path, output_path):
    output_paths = []
    for data_path in Path(input_path).rglob('*' + DATA_EXT):
        output_paths.append(str(data_path))

    sub_dirs = getSubDirsFromPaths(output_paths)
    output_paths = mergeOutputPathAndSubDirs(output_path, sub_dirs)
    
    return output_paths

def parseArgs():
    parser = argparse.ArgumentParser(DESCRIPTION)
    parser.add_argument(
        '--input-path',
        required=True,
        type=str,
        help='Path to file to be converted.')
    parser.add_argument(
        '--output-path',
        required=False,
        type=str,
        help='Path to directory to save updated data and labels. If no path is given then the output path will be the same as the input path.')
    parser.add_argument(
        '--output-data-file-name',
        required=True,
        type=str,
        help='Name of data files to save.')
    parser.add_argument(
        '--output-label-file-name',
        required=True,
        type=str,
        help='Name of label files to save.')
    parser.add_argument(
        '--label-column-index',
        required=False,
        default=-1,
        type=int,
        help='Index of the label column.')
    parser.add_argument(
        '--label-column-name',
        required=False,
        type=str,
        help='Name of the label column, will overwrite label-column-index.')
    parser.add_argument(
        '--time-column-name',
        required=False,
        default=TIME_HEADER,
        type=str,   
        help='Name of the time column.')
    parser.add_argument(
        '--time-column-index',
        required=False,
        default=None,
        type=int,
        help='Index of the time column.')
    parser.add_argument(
        '--hz',
        required=False,
        type=int,
        help='Add timestamps with the given frequency. Will overwrite time-column-name and time-column-index.')
    parser.add_argument(
        '--none-label',
        required=False,
        default=None,
        type=str,
        help='When training a model in Imagimob Studio it\'s require that one label is \'None\' label. This is the name of the label that will be converted to \'None\'. The none class represents the default state of the model and it\'s what the model should output when the other classes aren\'t triggered. I.e. using gesture, the none class is any data not linked to the specific gestures in question.')
    parser.add_argument(
        '--has-headers',
        required=True,
        type=str,
        help='If data has headers.')
    # parser.add_argument(
    #     '--use-dir-name-as-label',
    #     required=True,
    #     default=None,
    #     type=bool,
    #     help='')
    
    return parser.parse_args()

def main():
    args = parseArgs()
    output_path = args.output_path
    label_column_index = args.label_column_index
    label_column_name = args.label_column_name
    time_column_name = args.time_column_name
    time_column_index = args.time_column_index
    data_file_name = args.output_data_file_name
    label_file_name = args.output_label_file_name
    default_label = args.none_label
    has_headers = str2bool(args.has_headers)

    input_paths = get_data_file_paths(args.input_path, data_extension=DATA_EXT)

    data_frame_list = get_data_frame_list(args.input_path, has_headers)

    if has_headers is False:
        data_frame_list = addHeadersToDataFrames(data_frame_list, time_column_index)

    # If output path is none then save it in same directory as the input data
    if output_path is None:
        output_paths = get_output_paths_from_input_paths(args.input_path)
    else:
        output_paths = get_output_paths_from_input_and_output_path(input_paths, output_path)
    

    # Get label column name from index
    if label_column_name is None and label_column_index is not None:
        label_column_name = data_frame_list[0].columns[label_column_index]

    if time_column_name is None and time_column_index is not None:
        time_column_name = data_frame_list[0].columns[time_column_index]

    # Add timestamps to data with a given frequency
    if args.hz is not None:
        hz = args.hz
        data_frame_list = add_timestamps_to_data_frames(data_frame_list, hz)
        time_column_name = TIME_HEADER

    for data_frame, output_path in zip(data_frame_list, output_paths):
        labels_df = convert_row_based_labels_to_imagimob_labels(data_frame, label_column_name, time_column_name, default_label)
        data_df = data_frame.drop(label_column_name, axis=1)

        save_files(data_df, labels_df, output_path, data_file_name, label_file_name)

    return

if __name__ == '__main__':
    main()