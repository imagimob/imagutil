import argparse
import subprocess
import sys
from pathlib import Path


def main():
    args = parse_args()

    # Search for .data files
    data_root_path = Path(args.data_root)
    filename_wildcard = args.input_name
    input_file_paths = recursive_file_search(data_root_path, filename_wildcard)

    # Run processing script on all input files
    processing_script_path = args.processing_script
    output_name = args.output_name

    for input_file_path in input_file_paths:
        output_file_path = create_output_file_path(input_file_path, output_name)
        call_processing_script(processing_script_path, input_file_path, output_file_path)


def parse_args():
    parser = argparse.ArgumentParser("Run processing script on multiple files.")
    parser.add_argument(
        "-d", "--data-root",
        type=str,
        required=True,
        help="Root directory to search for .data files in.")
    parser.add_argument(
        "-i", "--input-name",
        type=str,
        required=True,
        help="Name of .data file to search for, can be wildcard format.")
    parser.add_argument(
        "-o", "--output-name",
        type=str,
        default=None,
        help="Name to give to output files. Defaults to <input-name>_processed.data.")
    parser.add_argument(
        "-p", "--processing-script",
        type=str,
        required=True,
        help="Path to processing script that is called with --input/--output argument.")
    return parser.parse_args()


def recursive_file_search(root_search_path: Path, filename_wildcard: str):
    return list(root_search_path.rglob(filename_wildcard))


def create_output_file_path(input_file_path: Path, output_name: str):
    if output_name is None:
        output_name = input_file_path.stem + "_processed" + input_file_path.suffix
    return input_file_path.parent / output_name


def call_processing_script(
        processing_script_path: Path,
        input_file_path: Path,
        output_file_path: Path):
    subprocess_call = [
        sys.executable,
        str(processing_script_path),
        "--input", str(input_file_path),
        "--output", str(output_file_path)
    ]
    subprocess_call_str = " ".join(subprocess_call)
    try:
        subprocess.check_output(subprocess_call)
    except subprocess.CalledProcessError:
        print(f"ERROR Command failed: {subprocess_call_str}")
    finally:
        print(f"SUCCESS Command completed successfully: {subprocess_call_str}")


if __name__ == '__main__':
    main()
