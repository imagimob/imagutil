import os
from pathlib import Path
from enum import Enum

import numpy as np
import pandas as pd
import scipy.ndimage

from .files import LABEL_EXT
from .headers import TIME_HEADER, LABEL_LENGTH_HEADER, LABEL_LABEL_HEADER
from .headers import LABEL_CONFIDENCE_HEADER, LABEL_COMMENT_HEADER


class ThresholdStrategy(Enum):
    """
    Defines thresholding strategies for generate_labels function
    """
    percentage = 0
    value = 1

    @staticmethod
    def list_names():
        return list(map(lambda c: c.name, ThresholdStrategy))

class DataProcessStrategy(Enum):
    """
    Defines processing strategies for multi-dimentional data
    """
    mean = 0
    sum = 1

    @staticmethod
    def list_names():
        return list(map(lambda c: c.name, DataProcessStrategy))

def generate_labels_recursively(
        root_input_dir: Path,
        label: str,
        label_file_name: str,
        data_process_strategy: DataProcessStrategy,
        data_file_wildcard: str,
        threshold_value: float,
        threshold_strategy: ThresholdStrategy,
        hanning_len: int,
        opening_steps: int,
        closing_steps: int):
    """
    Recursively searches for data files and generates labels for each file with given paramters.
    """
    data_files = search_for_data_files(root_input_dir, data_file_wildcard)

    if len(data_files) == 0:
        print(f"Could not find any data files with given path and wild card: {root_input_dir / data_file_wildcard}")
        return

    print("\n")
    print(f"Generating labels for {len(data_files)} files with parameters: ")
    print(f"File name: {label_file_name + LABEL_EXT}")
    print(f"Label name: {label}")
    print(f"Data process strategy: {data_process_strategy.name}")
    print(f"Threshold strategy: {threshold_strategy.name}")
    print(f"Threshold value: {threshold_value}")
    print(f"Hanning window length: {hanning_len}")
    print(f"Binary opening steps: {opening_steps}")
    print(f"Binary closing steps: {closing_steps}")
    print("\n")

    for data_file in data_files:
        label_file_path, label_count = generate_labels(
            data_file,
            label,
            label_file_name,
            data_process_strategy,
            threshold_value,
            threshold_strategy,
            hanning_len,
            opening_steps,
            closing_steps)
        print(f"Generated {label_count} labels for file: {label_file_path}")


def search_for_data_files(search_dir: Path, data_file_wildcard: str):
    """
    Searches recursively for files named according to data_file_wildcard
    """
    data_files = list(search_dir.rglob(data_file_wildcard))
    if not data_files:
        print(f"Warning: Could not find any data files named: {data_file_wildcard}")
    return data_files


def generate_labels(
        data_file: Path,
        label: str,
        label_file_name: str,
        data_process_strategy: DataProcessStrategy,
        threshold: float,
        threshold_strategy: ThresholdStrategy,
        hanning_len: int = 0,
        opening_steps: int = 0,
        closing_steps: int = 0):
    """
    Generate labels for a multi-dimentional signal with the following approach:
    - Process all feature values to get 1D feature signal
    - Apply hanning window smoothing
    - Threshold data to binary signal
    - Apply binary opening to remove noise in binary signal
    - Apply binary closing to close gaps in binary signal
    - Convert continous sequences of ones in binary signal to labels
    """
    data_df = pd.read_csv(data_file, index_col=0)
    feature_size = len(data_df.columns)
    #process data if data is more than 1D
    if  feature_size == 1:
        signal_column = data_df.columns[0]
        signal_raw = data_df[signal_column].values
    elif feature_size > 1:
        #process multi dementional features to 1d signal
        signal_raw = process_data(data_df,data_process_strategy)

    # Apply smoothing
    signal_processed = signal_raw
    if hanning_len > 0:
        hann_w = np.hanning(hanning_len)
        signal_processed = np.convolve(hann_w / hann_w.sum(), signal_processed, mode='same')

    # Threshold to binary data
    signal_binary = threshold_signal(signal_processed, threshold, threshold_strategy)

    # Apply binary opening to reduce noise in the binary signal
    if opening_steps > 0:
        signal_binary = scipy.ndimage.binary_opening(signal_binary, iterations=opening_steps)

    # Apply binary closing to close gaps in the binary signal
    if closing_steps > 0:
        signal_binary = scipy.ndimage.binary_closing(signal_binary, iterations=closing_steps)

    # Convert from binary to 0.0/1.0 float array
    signal_float = binary_signal_to_float_signal(signal_binary)

    # Make sure edges of signal is zero
    signal_float[0] = 0.0
    signal_float[-1] = 0.0

    # Find rising and falling edges
    signal_edges = np.diff(signal_float)
    rising_edges = np.where(signal_edges == 1.0)[0]
    falling_edges = np.where(signal_edges == -1.0)[0]

    # Assume rising and falling edges are of equal length (should always happen)
    assert len(rising_edges) == len(falling_edges), f"Error when generating labels for: {data_file}"

    # Create labels and write to file
    label_df = create_label_dataframe()
    for edge_index in range(len(rising_edges)):
        label_start_index = rising_edges[edge_index]
        label_end_index = falling_edges[edge_index]
        label_start_time = data_df.index[label_start_index]
        label_end_time = data_df.index[label_end_index]
        label_length = label_end_time - label_start_time
        add_label_row(label_df, label_start_time, label_length, label)

    recording_dir = data_file.parent
    label_file_path = write_label_file(label_df, recording_dir, label_file_name)
    label_count = len(label_df)
    return label_file_path, label_count

def process_data(data_df,data_process_strategy):
    """
    Process multi-dimentional data with given features processing strategy
    """
    if data_process_strategy == DataProcessStrategy.mean:
        # comput mean of all feature values
        processed_data = data_df.iloc[:,1:].mean(axis=1)
    elif data_process_strategy == DataProcessStrategy.sum:
        # sum up all feature values
        processed_data = data_df.iloc[:,1:].sum(axis=1)
    else:
        raise SystemExit("Invalid features processing strategy provided")
    return processed_data

def threshold_signal(signal, threshold, threshold_strategy):

    """
    Threshold signal with given threshold value and strategy
    """
    signal_binary = None
    if threshold_strategy == ThresholdStrategy.percentage:
        # Threshold based on percentage of max value
        signal_max = np.max(signal)
        signal_scaled = signal / signal_max
        signal_binary = signal_scaled > threshold
    elif threshold_strategy == ThresholdStrategy.value:
        # Threshold based on signal value
        signal_binary = signal > threshold
    else:
        raise SystemExit("Invalid threshold strategy provided")
    return signal_binary


def binary_signal_to_float_signal(signal_binary):
    """
    Converts binary array (true/false) to float array where:
      true -> 1.0
      false -> 0.0
    """
    signal_float = np.zeros(signal_binary.shape, dtype=np.float32)
    signal_float[signal_binary] = 1.0
    return signal_float


def create_label_dataframe():
    """
    Create a pandas.DataFrame object with the default Imagimob Studio labels format
    """
    columns = [TIME_HEADER, LABEL_LENGTH_HEADER, LABEL_LABEL_HEADER, LABEL_CONFIDENCE_HEADER, LABEL_COMMENT_HEADER]
    df = pd.DataFrame(columns=columns)
    df.set_index(TIME_HEADER, inplace=True)
    return df


def add_label_row(label_df: pd.DataFrame,
                  start_time: float,
                  length: float,
                  label: str,
                  confidence: float = 1.0,
                  comment: str= ""):
    """
    Add a row with a labels to a pandas.DataFrame.
    """
    label_df.loc[start_time] = [length, label, confidence, comment]


def write_label_file(label_df: pd.DataFrame, output_dir: Path, label_file_name: str = 'labels'):
    """
    Write labels DataFrame to file.
    """
    label_file_path = output_dir / (label_file_name + LABEL_EXT)
    label_df.to_csv(label_file_path)
    return label_file_path
